[//]: # TIP: Create the schedule in a temporary spreadsheet, and then copy/paste the rows into an online markdown generator (https://www.google.com/search?q=copy-table-in-excel-and-paste-as-a-markdown-table)

| Date       | Stage groups    |                               |                 |             | Host      |
| ---------- | --------------- | ----------------------------- | --------------- | ----------- | --------- |
| 2020-11-11 | Create          | Release: Progressive Delivery | Growth          | **Open**    | Taurie    |

| Date       | Host            |
| ---------- | --------------- |
| 2020-11-25 | Mike            |
| 2020-12-09 | Jacki           |
| 2021-01-06 | Justin          |
| 2021-01-20 | Marcel          |
| 2021-02-03 | TBD             |
| 2021-02-17 | Taurie          |
| 2021-03-03 | Mike            |
| 2021-03-17 | Jacki           |
| 2021-03-31 | Justin          |
| 2021-04-14 | Marcel          |
| 2021-04-28 | TBD             |
| 2021-05-12 | Taurie          |
| 2021-05-26 | Mike            |
| 2021-06-09 | Jacki           |
