---
layout: markdown_page
title: "Channel Services Program"
---
# GitLab Channel Services Program

The GitLab Channel Services program is designed to help new or existing partners design service portfolios around the DevOps Lifecycle in correlation to GitLab products. The program will help partners evaluate business opportunities and create a technical enablement framework to use as they build their GitLab-related service practices. 

The foundation of the GitLab Services Program consists of two elements on which partners can build upon to start or grow their GitLab-related service portfolio:



1. Certifications:  GitLab is delivering new GitLab Certified Service Partner Badges, that include training for service delivery teams which enable partners to meet program compliance requirements. 
2. Service Development Framework:  Partners can utilize GitLab packaged service kits to assist their teams as they build the service offerings, market and sell their services, and support their service business growth. 


## GitLab Certified Service Partner Program Overview

The GitLab Certified Service Partner Program offers our Partners three Certifications:


<table>
  <tr>
   <td style = "text-align: center">Available November 2020
   </td>
   <td style = "text-align: center">Available December 2020
   </td>
   <td style = "text-align: center">Available November 2020
   </td>
  </tr>
  <tr>
   <td style = "text-align: center">




<img src="images/Channel Service Program/gitlab-professional-services-partner.png" width="" alt="alt_text" title="image_tooltip">

   </td>
   <td style = "text-align: center">




<img src="images/Channel Service Program/gitlab-managed-services-partner.png" width="" alt="alt_text" title="image_tooltip">

   </td>
   <td style = "text-align: center">




<img src="images/Channel Service Program/gitlab-training-services-partner.png" width="" alt="alt_text" title="image_tooltip">

   </td>
  </tr>
  <tr>
   <td style = "text-align: center">GitLab Certified Professional Services Partner (PSP)
<br> 
<br>Distinguished partners with validated service capabilities  and a record for delivering excellent customer value through professional services.
   </td>
   <td style = "text-align: center">GitLab Certified Managed Services Partner (MSP)
<br> 
<br>Distinguished partners with validated service capabilities  and a record for delivering excellent customer value through managed services.
   </td>
   <td style = "text-align: center">GitLab Certified Training Partner (CTP)
<br> 
<br>Distinguished partners with validated learning and development capabilities and a record for delivering excellent customer experience through training services.
   </td>
  </tr>
</table>



<br>We have designed the GitLab Certified Service Partner Program to help partners who focus on offering services as a key foundation in their business model. These services can include:  



*   Implementation 
*   Migration 
*   Assessments & Audits
*   Infrastructure as Code/GitOps
*   DevSecOps 
*   Optimization & Transformation

Becoming an authorized GitLab partner is the first step to becoming a Certified Service Partner and allows you to engage with the GitLab sales team and resell or refer GitLab products to your customers. Partners can achieve the GitLab Service Partner Program badges in addition to their program status to differentiate through their service offerings and unlock both financial and non-financial incentives as they achieve Certified Service Partner Badges.

Whether you are a new partner just getting started with a service portfolio or you already have a thriving service business that you are looking to grow through offering services around GitLab, our Certified Service Partner Program will offer you opportunities to build a successful business with GitLab.
## GitLab Certified Service Partner Requirements

The GitLab Certified Service Partner framework is designed to give partners multiple paths to success by focusing on the service priorities that are aligned to their individual business model. 

Partners can achieve multiple certifications or focus on one category based on their chosen areas of focus and investment choices. 



<div align= "center"><img src="images/Channel Service Program/Q3FY21 GitLab Certified Service Partner Program.png" width="" alt="alt_text" title="image_tooltip"></div>



## GitLab Certified Service Partner Requirements and Progression

At GitLab, Certified Service Partner Certifications are meant to recognize service delivery excellence, technical expertise, and customer success among our partners for GitLab use cases in the DevOps lifecycle. Achieving a GitLab Service Partner Badge allows you to enhance your service capabilities and unlock key partner benefits in our Program. 



<div align = "center"><img src="images/Channel Service Program/Service Program Requirements and Progression.png" width="" alt="alt_text" title="image_tooltip"></div>

<br>
<h2>Becoming a Certified Service Partner</h2>

All GitLab Certified Service Partner tracks have program compliance and training requirements that must be completed before you can obtain the associated certification.

When the contractual requirements are met, and your organization sponsors the required number of practitioners who complete the training requirements and obtain the associated badges, your company will earn the related GitLab Service Partner Certification. 

The GitLab Channel team will communicate the award in email, and reflect the certification in the GitLab Partner Locator. 
<table>
   <tr>
      <td>

<h3>Technical Services</h3>

      </td>
      <td colspan="3" >· Reviewed annually

<br>· Non- compliant partners at the time of the annual review will have one quarter to return to compliance. 

      </td>
   </tr>
   <tr>
      <td>Open and Select GitLab Partners who design, build and operate a technical service practice can become PSPs if they (a)  perpetually employ:



<br>*   at least one (1) [GitLab Certified Sales Professional](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least one (1) [GitLab Certified Solution Architect](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least three (3) [GitLab Certified Professional Service Engineers](https://about.gitlab.com/handbook/resellers/training/), 

<br>and (b) maintain positive Customer Success ratings measured against the following [Time-to-Value KPIs](https://about.gitlab.com/handbook/customer-success/vision/#time-to-value-kpis) that GitLab uses for its metrics: Infrastructure Ready, First Value, & Outcome Achieved.

      </td>
      <td>

<br>
<br>
<br>
<br><img src="images/Channel Service Program/gitlab-professional-services-partner.png" width="" alt="alt_text" title="image_tooltip">


      </td>
      <td>



<br>
<br><img src="images/Channel Service Program/gitlab.png" width="" alt="alt_text" title="image_tooltip">
GitLab Software

<br>    +



<br><img src="images/Channel Service Program/cycle.png" width="" alt="alt_text" title="image_tooltip">
DevOps Services

      </td>
      <td>
      <br>
      <br>
      <br>GitLab Partner leveraging GitLab software to deliver customer value through Professional Service offerings. 

      </td>
   </tr>
   <tr>
      <td>Open and Select GitLab Partners who design, build and operate a technical service practice can become MSPs if they (a) perpetually employ:



<br>*   at least one (1) [GitLab Certified Sales Professional](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least one (1) [GitLab Certified Solution Architect](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least three (3) [GitLab Certified Managed Services Engineers](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least two (2) [GitLab Professional Service Engineer](https://about.gitlab.com/handbook/resellers/training/) 

<br>in their technical services delivery organizations, (b) complete the GitLab MSP contract exhibit, and (c)  maintain positive Customer Success ratings measured against the following [Time-to-Value KPIs](https://about.gitlab.com/handbook/customer-success/vision/#time-to-value-kpis) that GitLab uses for its metrics: Infrastructure Ready, First Value, & Outcome Achieved.

      </td>
      <td>
<br>
<br>
<br>
<br><img src="images/Channel Service Program/gitlab-managed-services-partner.png" width="" alt="alt_text" title="image_tooltip">


      </td>
      <td>



<br>
<br><img src="images/Channel Service Program/gitlab.png" width="" alt="alt_text" title="image_tooltip">
GitLab Software

<br>    +





<br><img src="images/Channel Service Program/software-development-lifecycle_original-manage-46px (1).png" width="" alt="alt_text" title="image_tooltip">
Managed Services

      </td>
      <td>
      <br>
      <br>
      <br>GitLab Partners leveraging GitLab software to deliver customer value through Professional Service offerings and/or Managed Services. 

   </td>
  </tr>
  <tr>
      <td>

<h3>Training  Services</h3>

      </td>
      <td colspan="3" >· By Invitation Only

<br>· Work with your CAM on a business case if you are interested

<br>· Non- compliant partners at the time of a review will have one quarter to return to compliance. 

      </td>
   </tr>
   <tr>
      <td>Open and Select GitLab Partners who design, build and operate a training service practice can become CTPs if they (a) perpetually employ:



<br>*   at least one (1) [GitLab Certified Sales Professional](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least one (1) [GitLab Certified Solution Architect](https://about.gitlab.com/handbook/resellers/training/) 
<br>*   at least two (2) [GitLab Certified Trainers](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-certified-trainer-process/) who each have two (2) badges from the [GitLab Train the Trainer Program,](https://about.gitlab.com/services/education/train-the-trainer/)

<br>(b) complete the GitLab CTP contract exhibit, and (c) ensure their training services practice maintains an average Customer Satisfaction rating of at least 80% for the GitLab licensed courses their organization delivers to our joint customers. 

GitLab Certified Training Partners can either act as subcontractors to [GitLab Education Services](https://about.gitlab.com/services/education/), or operate a training services practice that delivers GitLab Licensed Courseware directly to our joint customers. 

      </td>
      <td>

<br>
<br>
<br>
<br><img src="images/Channel Service Program/gitlab-training-services-partner.png" width="" alt="alt_text" title="image_tooltip">


      </td>
      <td>



<br>
<br><img src="images/Channel Service Program/gitlab.png" width="" alt="alt_text" title="image_tooltip">
[GitLab Ed Services](https://about.gitlab.com/services/education/) 

<br>      +






<br><img src="images/Channel Service Program/pencil.png" width="" alt="alt_text" title="image_tooltip">
Partner L&D Services

      </td>
      <td>
      <br>
      <br>
      <br>GitLab Partners leveraging GitLab licensed courseware to deliver customer value through Learning and Development Service offerings. 

      </td>
   </tr>
</table>


<br>
<h2>GitLab Certified Service Partner Program Benefits</h2>

There is significant business opportunity for partners to offer and deliver GitLab enabled services, and in order to ensure there is consistent customer experience throughout the market we offer Service Partners certifications.

Each Certification track offers unique benefits that help partners better prepare themselves to deliver customer value through services directly to our joint customers. 


<table>
  <tr>
   <td colspan="5" >
<h3>Training Benefits</h3>

GitLab recognizes the important role our partners play in delivering value and services to our joint customers. To ensure partners have the latest sales and technical knowledge about our products, we offer many different training opportunities. Partners can learn in a self-paced environment, on-line as well as various in-person, instructor-led classes. Training Benefits give partners the opportunity to pursue accreditations and certifications.
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Training Benefits and Resources
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>MSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="4" >Quarterly updates and training events help our partners achieve and maintain accreditations and certifications. 
   </td>
  </tr>
  <tr>
   <td colspan="4" >Training discounts for GitLab instructor-led courses. 
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="3" >Service Partner Certifications: Recognizes customer success performance for your organization
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="3" >Certifications: Recognize service capabilities among your delivery teams
   </td>
  </tr>
  <tr>
   <td colspan="5" >
<h3>Marketing Benefits</h3>

GitLab Open and Select partners have access to the proposal-based <a href="https://about.gitlab.com/handbook/resellers/#the-marketing-development-funds-mdf-program">GitLab Marketing Development Funds (MDF) Program</a>, which provides funding support for eligible marketing and partner enablement activities. Select partners who are also GitLab Certified Service Partners will have the highest visibility in the market. 
   </td>
  </tr>
  <tr>
   <td rowspan="5" >Services Related Marketing Benefits
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>MSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="4" >MDF is available to help partners promote service offerings.
   </td>
  </tr>
  <tr>
   <td colspan="4" >Partner Locator: find partners with capabilities to address specific outcomes.
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="3" >Badging (specific to the GitLab Certified Service Partner  track)
   </td>
  </tr>
  <tr>
   <td colspan="3" >
   </td>
   <td>LXP Class Scheduling (future): add public training events into our LXP for visibility to a broader customer base.
   </td>
  </tr>
  <tr>
   <td colspan="5" >
<h3>Sales Acceleration</h3>

All authorized GitLab partners are eligible for co-selling with the GitLab Sales team. Certified partners are often prioritized in co-selling activities because those partners are able to deliver greater value for our joint customers. The GitLab Sales team and partner sellers align to drive customer value through joint account planning, account development, and working together to grow revenue.  Additionally, Certified Service Partners can be brought in for services only opportunities to help customers assess their DevOps infrastructure in advance of a potential GitLab purchase, or optimize their GitLab deployments.
   </td>
  </tr>
  <tr>
   <td rowspan="8" >Sales Acceleration
   </td>
   <td>Open/Select
   </td>
   <td>PSP
   </td>
   <td>MSP
   </td>
   <td>CTP
   </td>
  </tr>
  <tr>
   <td colspan="4" >Introduction to GitLab sales team for account planning
   </td>
  </tr>
  <tr>
   <td colspan="4" >Introduction to sales opportunities by the GitLab Sales team
   </td>
  </tr>
  <tr>
   <td colspan="4" >Internal Use Licenses: Offering discounted licenses for internal use
   </td>
  </tr>
  <tr>
   <td colspan="4" >No cost Not for Resale (NFR) licenses: for lab testing, demos, training and educational use incremental NFRs given to PSP and MSP
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="3" >Service Packages : GitLab Services toolkits, sales and marketing IP
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td colspan="3" >Opportunities for co-delivering services
   </td>
  </tr>
  <tr>
   <td colspan="3" >
   </td>
   <td>
   80% discount on licensed courseware
   </td>
  </tr>
</table>



<br>
<h2>Maintaining Service Partner Certifications</h2>

At GitLab, collaboration and feedback is meant to nurture and mentor our certified service partners to grow their service capabilities and expertise. The program will utilize our internal [customer success metrics](https://about.gitlab.com/handbook/customer-success/vision/#measurement-and-kpis) to measure and understand the GitLab customer experience across the entire ecosystem and help further develop our certified service partners. In order to work together in this process, we will ask you to provide context to the annual customer success report for your customer set one month before it is due to be published. 

GitLab Channel Partner program will review the GitLab Certified Service Partners’ customer success rating and practitioner certification status each year in the month the partner was originally granted certification. Partners will be notified with the outcome of the review and the resulting status of their certification. If the partner needs to make further investments to stay in good standing with regards to Service Partner Certifications, the email will indicate the existing status and state the expected status with a deadline when compliance is required to renew your GitLab Service Partner Certification. Partners can work with their GitLab Channel Account Manager (CAM) to create a plan for obtaining and maintaining certifications.
