---
layout: handbook-page-toc
title: "Targeted Sales & Customer Success Roles for Sales Quick Start (SQS)"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Targeted Sales & Customer Success Roles for Sales Quick Start (SQS)
*  Targeted roles for the Sales Quick Start learning path include recently hired: 
   - Enterprise and Public Sector Strategic Account Leaders (SALs)
   - Mid-Market Account Executives (AEs)
   - SMB Customer Advocates (CAs)
   - Inside Sales Reps (ISRs)
   - Solution Architects (SAs) 
   - Technical Account Managers (TAMs) 
   - Professional Services Engineers (PSEs) (PSEs do not participate in the Sales Quick Start Workshop)
   - Professional Services Engagement Managers and Practice Managers
   - Area Sales Managers (ASMs)
   - Regional Directors (RDs) 
   - Channels (all roles)
   - Alliances (all roles)

Sales Development Reps (SDRs) have their own separate onboarding process (but many of the same elements are shared) and they attend SQS Workshops as well.
