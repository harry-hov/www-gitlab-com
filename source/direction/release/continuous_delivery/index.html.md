---
layout: markdown_page
title: "Category Direction - Continuous Delivery"
description: We follow the well-known definitions from Martin Fowler on the difference between continuous delivery and continuous deployment. Learn more here!
canonical_path: "/direction/release/continuous_delivery/"
---

- TOC
{:toc}

## Continuous Delivery

Many teams are reporting that development velocity is stuck; they've
reached a plateau in moving to more and more releases per month, and now need help on how to improve. According to analyst research, 40% of software development
team's top priorities relate to speed/automation, so our overriding vision
for Continuous Delivery is to help these teams renew their ability to
accelerate delivery.

Additionally, Continuous Delivery serves as the "Gateway to Operations"
for GitLab, unlocking the downstream features such as the [Configure](/direction/ops/#configure)
and [Monitor](/direction/ops/#configure) stages.

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContinuous%20Delivery)
- [Overall Vision](/direction/ops/#release)
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aprogressive%20delivery&label_name[]=Category%3AContinuous%20Delivery)
- [Research insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3AContinuous%20Delivery)
- [Documentation](https://docs.gitlab.com/ee/ci/)

### Continuous Delivery vs. Deployment

We follow the well-known definitions from Martin Fowler on the
difference between continuous delivery and continuous deployment:

- **Continuous Delivery** is a software development discipline
 where you build software in such a way that the software can
 be released to production at any time.
- **Continuous Deployment** means that every change goes through the
 pipeline and automatically gets put into production, resulting in many
 production deployments every day.
In order to do Continuous Deployment, you must be doing Continuous Delivery.

_Source: [https://martinfowler.com/bliki/ContinuousDelivery.html](https://martinfowler.com/bliki/ContinuousDelivery.html)_

### Infrastructure Provisioning

Infrastructure Provisioning and Infrastructure as Code, using solutions like Terraform or other provider-specific methods, is an interesting topic that relates to deployments but is not part of the Continuous Delivery category here at GitLab. For details on solutions GitLab provides in this space, take a look at the [category page](/direction/configure/infrastructure_as_code/) for our Infrastructure as Code team.

### Deployment with Auto DevOps

For deployment to Kubernetes clusters, GitLab has a focused category called Auto DevOps which is oriented around providing solutions for deploying to Kubernetes. Check out their [category page](/direction/configure/auto_devops/) for details on what they have planned.

We are working on a similar experience for non Kubernetes users, starting with [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351) that will automatically detect when users are deploying to AWS and will connect the dots for them.

The [Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/#auto-deploy) [jobs](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib%2Fgitlab%2Fci%2Ftemplates%2FJobs) within [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) are maintained by the Continuous Delivery category.

## What's Next & Why

As part of [Post-deployment monitoring MVC](https://gitlab.com/groups/gitlab-org/-/epics/3088), we are also adding the ability to rollback automatically in case a critical alert is raised via [gitlab#35404](https://gitlab.com/gitlab-org/gitlab/-/issues/35404).

As part of our efforts to [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351), we are adding the option to Deploy to AWS EC2 using AutoDevOps via [gitlab#216008](https://gitlab.com/gitlab-org/gitlab/-/issues/216008)

## Maturity Plan

This category is currently at the "Complete" maturity level, and our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

 **CI.yaml features**
- [Limit pipeline concurrency using named semaphores](https://gitlab.com/gitlab-org/gitlab/issues/15536) (Complete)
- [Group deploy tokens](https://gitlab.com/gitlab-org/gitlab/issues/21765) (Complete)
- [Allow only forward deployments](https://gitlab.com/gitlab-org/gitlab/issues/25276)  (Complete)
- [Allow Deploy keys to push to protected branches](https://gitlab.com/gitlab-org/gitlab/-/issues/30769) (Complete)
- [Group Deploy Keys](https://gitlab.com/gitlab-org/gitlab/issues/14729) (Complete)
- [Allow fork pipelines to run in parent project](https://gitlab.com/groups/gitlab-org/-/epics/3278)


**Cloud Deployments**
- [Streamline AWS Deployments](https://gitlab.com/groups/gitlab-org/-/epics/2351)

**Observability**
- [Post-deployment monitoring MVC](https://gitlab.com/groups/gitlab-org/-/epics/3088)
  * We recently recorded a [Think Big](https://youtu.be/zMmmAhrOIDs) session describing the vision for this.
- [Actionable CI/CD metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/7838)

**Auto Deploy (AutoDevOps Flow)**
- [Customize readinessProbe and livenessProbe for Auto DevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/22327)
- [Auto DevOps rollout is not compatible with Kubernetes 1.16](https://gitlab.com/gitlab-org/gitlab/-/issues/209045)

## Competitive Landscape

Because CI and CD are closely related, the [competitive analysis for Continuous Integration](/direction/verify/continuous_integration#competitive-landscape)
is also relevant here. For how CD compares to other products in the market,
especially as it relates to pipelines themselves, also take a look there.

### Microsoft/GitHub

As far as CD specifically, Microsoft has always been strong in providing actionable metrics on
development, and as they move forward to integrate GitHub and Azure
DevOps they will be able to provide a wealth of new metrics to help
teams using their solution. We can stay in front of this by improving
our own actionable delivery metrics in the product via [gitlab#7838](https://gitlab.com/gitlab-org/gitlab/-/issues/7838).

### Harness

[Harness](https://harness.io/) (with their recent acquisition of [Drone.io](https://harness.io/continuous-integration/)) is a modern, cloud-native CI/CD platform that provides excellent solutions for delivering to cloud environments using native approaches like Kubernetes. Harness also covers what it calls Continuous Verification - which integrates with existing Monitoring tools (like DataDog, Splunk or CloudWatch) and serves both for [verifying advanced deployments](https://gitlab.com/gitlab-org/gitlab/issues/8295) as well as an environment monitoring tool they call 24/7 Service Guard. Harness provides a more visual and template based CD pipeline definition process than GitLab.

We have researched [Harness](https://gitlab.com/gitlab-org/gitlab/-/issues/20307) and provide a comparison between [GitLab and Harness](/devops-tools/harness-vs-gitlab.html). We invite you to chime in on the issues and provide your insights and feedback as well.

### Spinnaker

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes.
It combines a powerful and flexible pipeline management system with integrations to the major cloud providers and treats deployments as a first-class citizen. However,
it also has a weakness that it does not support other stages of the DevOps lifecycle (such as Continuous Integration), while GitLab offers a single tool for your Development needs.

Our goal is to do make it so anything that Spinnaker can do, can also be done via built-in features in GitLab CD.

Spinnaker's advantage points are:
* Provides easy application deployment across cloud providers. We have also recognised this as one of our top vision items and are working to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804).
* Supports several deployment strategies which we are working on expanding in [Advanced Deploys](https://gitlab.com/groups/gitlab-org/-/epics/2213).
* Allows configuring an environment in standby for easy rollback, which is closely tied to [gitlab#35409](https://gitlab.com/gitlab-org/gitlab/issues/35409).
* Provides a user interface that allows you to view your infrastructure and to see exactly where your code is residing.
* Acts as an operator that perform rollback when needed without the need to provide a script to do so (similar to Kubernetes).
* Ablility to manage [multiple Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/#multiple-kubernetes-clusters), which is also supported by GitLab.

One analysis of GitLab vs. Spinnaker can be found on our [product comparison page](/devops-tools/spinnaker-vs-gitlab.html). There is another high quality one from our VP of product strategy in [gitlab#197709](https://gitlab.com/gitlab-org/gitlab/issues/197709#note_274765321). Finally, our PM for this section completed one where which can be found at [gitlab#35219](https://gitlab.com/gitlab-org/gitlab/issues/35219).
Our next step is to review these and combine in this section of this document as the single source of truth, exhaustively listing the issues we plan to deliver to reach functional parity (or better) with Spinnaker.

We also respect our customers choice to use Spinnaker's CD solution together with GitLab and are working on making
that integration easier with [gitlab#120085](https://gitlab.com/gitlab-org/gitlab/issues/120085).

### Waypoint

Waypoint, by Hashicorp, provides a modern workflow to build, deploy, and release across platforms. Waypoint uses a single configuration file and common workflow to manage and observe deployments across platforms such as Kubernetes, Nomad, EC2, Google Cloud Run, and more. It maps artifacts to runtime after the test and build phases. Waypoint sits alongside the source code, and enables declarative deployment - it takes the manifest of how the platform is configured and will execute the steps sequentially unique for each platform. Waypoint can be used with GitLab CI and even with the Auto DevOps workflow with deployments being done by Waypoint. 

## Analyst Landscape

In our conversations with industry analysts, there are a number of key trends
we're seeing happening in the CD space:

### Support a breadth of platforms, both legacy and cloud-native.

Cloud adoption of CI/CD is growing, extending capabilities for deploying to cloud environments, including Kubernetes and other modern container
architectures are a key metric. While cloud migration is accelerating and more teams are adopting it, on-premises and
legacy hardware environments remain.

We invite you to follow our plans to [natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804) and  [Serverless](https://about.gitlab.com/direction/configure/serverless/) to offer feedback or ask questions.

### Inject insight and analytics into pipelines.

Users are looking for the ability to not just measure platform stability and other
performance KPIs post-deployment, but also providie functionality such as automated release-readiness scoring
based on analysis of data from across the digital pipelines.
Tracking and measuring customer behavior, experience, and financial impact, after deployment via [gitlab#37139](https://gitlab.com/gitlab-org/gitlab/issues/37139) solves an important
pain point.

### Progressive Delivery

Progressive Delivery allows you to deploy code incrementally and target the audience that will receive the new code based on user segments and environments. By doing so, it enables experimentation with reduced risk. Progressive Delivery builds on the foundations laid by Continuous Integration and Continuous Delivery. Related categories to this theme are [Feature Flags](https://about.gitlab.com/direction/release/feature_flags/) and [Advanced Deployments](https://about.gitlab.com/direction/release/advanced_deployments/).
To read more about this see [RedMonk's post](https://redmonk.com/jgovernor/2019/07/10/progressive-delivery-at-gitlab/).

## Top Customer Success/Sales Issue(s)

The ability to monitor deployments and automatically halt/rollback deployment in case of exceeding a specific error rate is frequently mentioned by CS and in the sales cycle as a feature teams are looking for. This will be
implemented via [gitlab&3088](https://gitlab.com/groups/gitlab-org/-/epics/3088). We started by showing alerts on the environments page, in case an error threshold is crossed in [gitlab#214634](https://gitlab.com/gitlab-org/gitlab/-/issues/214634)(complete) and will iterate on this by adding the ability to rollback automatically in case a critical alert is raised via [gitlab#35404](https://gitlab.com/gitlab-org/gitlab/-/issues/35404).

## Top Customer Issue(s)

Our most popular customer issue is [gitlab#35779](https://gitlab.com/gitlab-org/gitlab/-/issues/35779) which allows replacing the user that is connected to a deploy key in case that user is invalid. Due to a security change that was done in release 12.4, the `git push` command stopped working for many scripts because the deploy keys were linked to expired or invalid users. This issue will eliminate the need to manually regenerate and/or replace the keys in each one of the scripts.

## Top Internal Customer Issue(s)

Adding a check for maximum commits before merge via [gitlab#26691](https://gitlab.com/gitlab-org/gitlab/-/issues/26691) is our most popular internal issue. This adds a validation check, to make sure that your merge request is not too far behind master before merging in order to avoid breaking master

## Top Vision Item(s)

Our top vision item is to [Natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804), and specifically deploying to [AWS](https://gitlab.com/groups/gitlab-org/-/epics/2351) we want to help make it easier and quicker to get started and deploy to any one of the big cloud providers using GitLab's CI/CD.

