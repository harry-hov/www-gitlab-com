---
layout: handbook-page-toc
title: "GitLab Onboarding Buddies"
description: "Onboarding Responsibilities and Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Onboarding Buddies

Onboarding buddies are crucial to making the onboarding experience for a new GitLab team-member a positive one. [New Job Anxiety](http://www.classycareergirl.com/2017/02/new-job-anxiety-conquer/) is a reality for many people, and the adjustment to GitLab might be particularly challenging for new GitLab team-members who may not be used to our [all-remote](/company/culture/all-remote/) culture. That's why it's important that all new GitLab team-members be assigned a buddy who is ready, willing, and excited to assist with the onboarding process.


## Buddy Responsibilities

1. **The first and most important thing an onboarding buddy should do is schedule a call with the new GitLab team-member.** We attempt to match (as best as possible, anyway) time zones between the new GitLab team-member and their onboarding buddy so that as soon as the new GitLab team-member logs on, you, the onboarding buddy, can be there ready and waiting to welcome them to the team.
1. **Check how far the new GitLab team-member has gotten in their onboarding issue.** The onboarding issue that all new GitLab team-members are assigned can be overwhelming at first glance, particularly on the first day of work. Check to see how much, if any, the new GitLab team-member has done by the time your call happens, and offer some direction or advice on areas the new hire may be having trouble with.
1. **Suggest helpful handbook pages.** Chances are that you've discovered some particularly helpful pages in the handbook during your time at GitLab. Point them out to the new GitLab team-member, and help them get used to navigating the handbook. Some examples might include:
     * [GitLab's guide for starting a remote role](/company/culture/all-remote/getting-started/)
     * [The tools page](/handbook/tools-and-tips)
     * [The team chart](/company/team/org-chart)
     * [The positioning FAQ](/handbook/positioning-faq)
1. **Remind them about introducing themselves.** Remind the new team member to introduce themselves in the Slack channel `#new_team_members`. Encourage them to write a little personal note, and if they're comfortable, include a photo or two!
1.  **Encourage them to organize a group call with other new hires.** New GitLab team-members who are used to (or prefer) a more conventional new hire orientation — frequently hosted in group settings in colocated organizations — [may feel a lack of early bonding](/company/culture/all-remote/learning-and-development/). Encourage them to organize a group call with other new hires in order to walk through onboarding together, while learning about new personalities and [departments of the company](/company/team/structure/).
1. **Introduce them to Slack.** Slack may seem like it's ubiquitous, but that doesn't necessarily mean the new GitLab team-member will have had experience using it before. Since it's a central part of how we communicate at GitLab, consider showing them around, and give them some pointers about [how we use it](/handbook/communication/#chat). 
  
     * Be sure to suggest [location channels](/handbook/communication/chat/#location-channels-loc_) and [Social Slack Groups](/handbook/communication/chat/#social-groups) where they can immediately plug in with other team members who appreciate similar things.
1. **Ask where they need help and connect them with the experts**. Onboarding buddies should make the effort to connect new GitLab team-members with subject matter experts if your assigned team member requests additional help in a given area. Examples are below.
     * For new GitLab team-members who have not worked in a [remote organization](/company/culture/all-remote/) before, they may need assistance in thinking through an ideal [workspace](/company/culture/all-remote/workspace/) and embracing [informal communication](/company/culture/all-remote/informal-communication/). Consider asking seasoned remote colleagues in the `#remote` Slack channel to reach out and answer questions.
     * If they're new to [Git](/training/), consider asking experts in the `#git-help` Slack channel to reach out and offer a tutorial.
     * If they're new to [Markdown](/blog/2018/08/17/gitlab-markdown-tutorial/), consider asking experts in the `#content` Slack channel to reach out and offer support.
1. **Help with the team page.** For less technical new hires, adding themselves to the [team page](/company/team/) might feel like the most daunting task on the onboarding issue. Offer to help with the process. This doesn't necessarily have to happen on day one, but you should let them know that you're available to help if and when they need it. Consider scheduling a second meeting later in the week to walk them through [the process](/handbook/git-page-update/#11-add-yourself-to-the-team-page)
  * In particular, help them to create their SSH key as this tends to be a sticking point for many new hires. You can also show them [Lyle's walkthrough](https://youtu.be/_FIOhk03VtM).
1. **Check in regularly.** You may very well be the first friend the new GitLab team-member makes on the team. Checking in with them regularly will help them feel welcome and supported. Reach out via Slack, and schedule at least one follow-up call for the week after their start date.
1. **Provide backup if needed**. If you plan to be out (e.g. [vacation](/handbook/paid-time-off/), [company business](/handbook/travel/), [events](/events/), etc.) during a new GitLab team-member's first few weeks, please ensure that a backup onboarding buddy is available to offer support.

## Buddy Program

In an effort to recognize buddies who go above and beyond to support new team members and help them feel welcome, we have implemented a Buddy Program. Once every quarter, the People Experience team will evaluate the onboarding buddy feedback provided in the [Onboarding Survey](https://docs.google.com/forms/d/1sigbOqWKuEtGyLROghvivgWErRnfbUI1_-57XhAwu_8/edit) filled out by new team members after their first 30 days at GitLab. To qualify,m the onboarding buddy needs to receive a score of 3 or higher to be entered into a raffle at the end of the quarter. 

The People Experience team will randomly select 3 winners. All 3 winners will receive a $22 discount code to use in the GitLab swag store.

- If the new team member does not complete the **Onboarding Survey**, the onboarding buddy will not be entered into the raffle. 
- In case of a buddy being selected twice, a re-draw will take place. 


## How Can I Become An Onboarding Buddy?

Buddy assignation is the new team member's manager's responsibility as outlined in the [onboarding issue](https://gitlab.com/gitlab-com/people-group/employment-templates-2/-/blob/master/.gitlab/issue_templates/onboarding.md) as the very first `Before Starting at GitLab` manager task. If you have been at GitLab 3+ months and are interested in participating in the Onboarding Buddy Program, please express your interest to your manager.

## That's it!

That's all there is to it! Thanks for your help welcoming the newest GitLab team-member to the team and getting them on board. If you have questions that are not answered on this page, please [reach out to People Ops](/handbook/people-group/)!

## Procedures 

### Onboarding Buddies Procedures for People Experience


The onboarding buddy scores are stored within the the OSAT Survey [results](https://docs.google.com/spreadsheets/d/1sAaQntIaQAnj8Z1NY6WRyQGRIyIoKa_6TratKWtScdo/edit#gid=63110344). Rows 418-494 were utilized for the Q1 Winner results. 

To update the swag code for the next quarter winners; 
1. Post a request in the `#Swag` slack channel 
1. Tag `@advocates`
1. Identify the amount and how many times it can be used (2 time = 2 winners)

Once a Winner/Winners have been identified post a message to the `Whats-happening-at-gitlab` channel and tag the winner/winners. 


